import { ObjectID } from 'mongodb';
export declare class Message {
    _id: ObjectID | string;
    markdown: string;
    roomId: ObjectID | string;
    sender: ObjectID | string;
    timestamp: string;
    constructor(message?: Message);
}
