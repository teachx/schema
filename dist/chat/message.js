"use strict";
exports.__esModule = true;
var Message = /** @class */ (function () {
    function Message(message) {
        if (message) {
            this._id = message._id;
            this.markdown = message.markdown;
            this.roomId = message.roomId;
            this.sender = message.sender;
            this.timestamp = message.timestamp;
        }
    }
    return Message;
}());
exports.Message = Message;
//# sourceMappingURL=message.js.map