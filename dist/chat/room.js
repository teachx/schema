"use strict";
exports.__esModule = true;
var ChatRoom = /** @class */ (function () {
    function ChatRoom(chatRoom) {
        this.members = [];
        if (chatRoom)
            this.members = chatRoom.members;
    }
    return ChatRoom;
}());
exports.ChatRoom = ChatRoom;
//# sourceMappingURL=room.js.map