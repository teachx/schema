"use strict";
exports.__esModule = true;
var User = /** @class */ (function () {
    function User() {
        this.name = '';
        this.email = '';
        this.password = '';
        this.role = 'teacher';
    }
    return User;
}());
exports.User = User;
//# sourceMappingURL=user.js.map