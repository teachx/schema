"use strict";
exports.__esModule = true;
var Job = /** @class */ (function () {
    function Job(job) {
        this.helpers = [];
        this.assignedHelpers = [];
        this.complete = false;
        this.status = 'pending';
        if (job) {
            this.creator = job.creator;
        }
    }
    return Job;
}());
exports.Job = Job;
//# sourceMappingURL=job.js.map