import { Job } from './job';
export declare class SafeSpace extends Job {
    duration: number;
    recurring: boolean;
    students: 'few' | 'classroom' | 'school';
    issue: {
        cyberbullying: boolean;
        lgbtq: boolean;
        other: boolean;
        physical: boolean;
        relationship: boolean;
        sexual: boolean;
        verbal: boolean;
    };
    constructor(space?: SafeSpace);
}
