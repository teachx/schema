"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var job_1 = require("./job");
var Project = /** @class */ (function (_super) {
    __extends(Project, _super);
    function Project(project) {
        var _this = _super.call(this, project) || this;
        _this.duration = 2;
        _this.expertType = 'guest';
        _this.locationType = 'school';
        _this.number = 40;
        _this.person = 'tutor';
        _this.recurring = false;
        _this.type = 'instructional';
        if (project) {
            _this.date = project.date;
            _this.description = project.description;
            _this.duration = project.duration;
            _this.expertType = project.expertType;
            _this.locationType = project.locationType;
            _this.students = project.students;
            _this.location = project.location;
            _this.number = project.number;
            _this.person = project.person;
            _this.recurring = project.recurring;
            _this.status = project.status;
        }
        return _this;
    }
    return Project;
}(job_1.Job));
exports.Project = Project;
//# sourceMappingURL=project.js.map