import { Job } from './job';
export declare class Project extends Job {
    duration: number;
    expertType: 'guest' | 'expert';
    locationType: 'physical' | 'virtual' | 'school';
    location: string;
    number: number;
    students: number;
    other?: string;
    person: 'tutor' | 'expert' | 'field-trip' | 'other';
    adminCategory: 'grade-work' | 'class-assistance' | 'other';
    adminSubject: 'math' | 'english' | 'science' | 'history';
    adminHelp: 'cleaning' | 'decorating' | 'monitoring' | 'general';
    techCategory: 'professional-development' | 'codding-kids' | 'other';
    techTraining: 'software' | 'hardware' | 'web-mobile' | 'integrating-tech' | 'other';
    techCodingLevel: 'beginner' | 'intermediate' | 'advance';
    recurring: boolean;
    constructor(project?: Project);
}
