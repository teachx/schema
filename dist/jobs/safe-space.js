"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var job_1 = require("./job");
var SafeSpace = /** @class */ (function (_super) {
    __extends(SafeSpace, _super);
    function SafeSpace(space) {
        var _this = _super.call(this, space) || this;
        _this.duration = 2;
        _this.recurring = false;
        _this.students = 'few';
        _this.issue = {
            cyberbullying: false,
            lgbtq: false,
            other: false,
            physical: false,
            relationship: false,
            sexual: false,
            verbal: false
        };
        _this.type = 'safespace';
        if (space) {
            _this.date = space.date;
            _this.description = space.description || '';
            _this.duration = space.duration || 2;
            _this.recurring = space.recurring || false;
            _this.students = space.students || 'few';
            _this.issue = {
                cyberbullying: space.issue.cyberbullying || false,
                lgbtq: space.issue.lgbtq || false,
                other: space.issue.other || false,
                physical: space.issue.physical || false,
                relationship: space.issue.relationship || false,
                sexual: space.issue.sexual || false,
                verbal: space.issue.verbal || false
            };
        }
        return _this;
    }
    return SafeSpace;
}(job_1.Job));
exports.SafeSpace = SafeSpace;
//# sourceMappingURL=safe-space.js.map