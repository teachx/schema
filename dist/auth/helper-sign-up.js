"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var teacher_sign_up_1 = require("./teacher-sign-up");
var HelperSignUp = /** @class */ (function (_super) {
    __extends(HelperSignUp, _super);
    function HelperSignUp() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.ssn = '';
        _this.dob = '';
        return _this;
    }
    return HelperSignUp;
}(teacher_sign_up_1.TeacherSignUp));
exports.HelperSignUp = HelperSignUp;
//# sourceMappingURL=helper-sign-up.js.map