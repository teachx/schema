# TeachX Schema

Documentation for updating the structured data schemas for the TeachX Server and Front-end application. Built using extensible, object-oriented classes in TypeScript.


## Install

1. Make sure you have Typescript installed: `npm install -g typescript`.

2. Clone this repo and `cd` to the appropriate directory.

3. Run `npm install` or `npm i`.


## Updating Schemas

1. `cd` to the appropriate directory for this repo on your local machine. Schemas are located in the `src` directory. To edit the properties of a schema, open the appropriate file in your IDE (e.g. `user.ts`), make the edits, then save.

2. Run `npm run prebublish` or `tsc` in the terminal. This will compile all edits and store them in the `dist` directory.

3. Commit the changes using `git add` and `git commit`.

4. Push the changes back up to this repo.

