export class ChatRoom {
    _id: any;
    members: any[] = [];

    constructor(chatRoom?: ChatRoom) {
        if (chatRoom) this.members = chatRoom.members;
    }
}
