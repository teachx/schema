import { ObjectID } from 'mongodb';

export class Message {
    _id: ObjectID | string;
    markdown: string;
    roomId: ObjectID | string;
    sender: ObjectID | string;
    timestamp: string;

    constructor(message?: Message) {
        if (message) {
            this._id = message._id;
            this.markdown = message.markdown;
            this.roomId = message.roomId;
            this.sender = message.sender;
            this.timestamp = message.timestamp;
        }
    }
}
