import { User } from "../user";

export class Job {
    _id: any;
    creator: any;
    teacher: string;
    type: 'instructional' | 'safespace' | 'admin' | 'technical';
    school: string;
    helpers: any[] = [];
    assignedHelpers: any[] = [];
    assignedHelperData: User[];
    deferredHelpers?: any[]
    rated: {
        user: any
        rating: number
        message?: string
    }[]
    date: string;
    enddate: string;
    time: string;
    description: string;
    timestamp: string;
    updated: string;
    complete: boolean = false;
    status: 'pending' | 'active' | 'complete' = 'pending';
    location: string;
    lat: number;
    lng: number;
    coords: {
        type: string;
        coordinates: number[]
    }

    constructor(job?: Job) {
        if (job) {
            this.creator = job.creator;
        }
    }
}
