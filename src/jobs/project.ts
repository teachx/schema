import { Job } from './job'
export class Project extends Job {
    duration = 2;
    expertType: 'guest' | 'expert' = 'guest';
    locationType: 'physical' | 'virtual' | 'school' = 'school';
    location: string;
    number: number = 40;
    students: number;
    other?: string;
    person: 'tutor' | 'expert' | 'field-trip' | 'other' = 'tutor';
    adminCategory: 'grade-work' | 'class-assistance' | 'other';
    adminSubject: 'math' | 'english' | 'science' | 'history';
    adminHelp: 'cleaning' | 'decorating' | 'monitoring' | 'general';
    techCategory: 'professional-development' | 'codding-kids' | 'other';
    techTraining: 'software' | 'hardware' | 'web-mobile' | 'integrating-tech' | 'other';
    techCodingLevel: 'beginner' | 'intermediate' | 'advance';
    recurring = false;

    constructor(project?: Project) {
        super(project);
        this.type = 'instructional';
        if (project) {
            this.date = project.date;
            this.description = project.description;
            this.duration = project.duration;
            this.expertType = project.expertType;
            this.locationType = project.locationType;
            this.students = project.students;
            this.location = project.location;
            this.number = project.number;
            this.person = project.person;
            this.recurring = project.recurring;
            this.status = project.status;
        }
    }

}
